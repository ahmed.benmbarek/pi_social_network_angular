import { Component, OnInit } from '@angular/core';
import { ReclamationService, Message } from '../Services/reclamation.service';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/scan';
import { FormGroup, FormControl, Validators } from '@angular/Forms';
import { User } from '../Model/user';
import { map, startWith } from 'rxjs/operators'
import { PayementService } from '../Services/payement.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  messages: Observable<Message[]>;
  textM: string;

  username = "ahmed"
  showClaimModal = false;
  showsignalModal=false;
  showBilling=false

  users : User[]
  filteredOptions: Observable<string[]>
  serachControl = new FormControl()
  options: string[] = []
  userIndex =0
  hideTextZone=true
  placeholderText="cette zone est désactivée pour le moment"
  spinner=true
  claimOrSignal =""
  confirmationMessage =""
  constructor(public reclamationService: ReclamationService,public payementService :PayementService ) { }

  ngOnInit() {
    this.reclamationService.getUsers().subscribe(res =>{
      this.users = res
    })

 
    
    /*this.filteredOptions = this.serachControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    )*/


    // appends to array after each new message is added to feedSource
    this.messages = this.reclamationService.conversation.asObservable().scan((acc, val) => acc.concat(val) );
    this.spinner = this.reclamationService.spinnerShow
    console.log(this.spinner)

  }
  initializeFilter(){
    this.filteredOptions = this.serachControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    )
    
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase()
    return this.options.filter(option =>
      option.toLowerCase().includes(filterValue)
    )
  }

 





  messageForm = new FormGroup({
    textMessage : new FormControl('',Validators.required)
    
  })
  get  textMessageC(){

    return this.messageForm.get('textMessage')

  }
  sendMessage(text:string) {
    this.reclamationService.converse(text);
    
    //console.log(this.messages)
    this.textM = '';
  }
  demarrer(){
  this.sendMessage("Démarrer")

  this.users.forEach(element => {
    this.options.push(element.nom)
    console.log(this.options)
  });  

this.initializeFilter()
 
  }
  redemarrer(){
    this.sendMessage("Redémarrer");
    
  }


  /******************************reclamation*********************************** */

    
  reclamer(){
    this.sendMessage("Reclamer");
   // this.claimOrSignal = "reclamation"
  }
  
  signaler(){
    this.sendMessage("Demande de signalisation")
   // this.claimOrSignal = "signalisation"
    console.log(this.claimOrSignal)
  }
  Choisir(){
this.showClaimModal=true

  }
  choix_de_societe(index:any){
    this.showClaimModal=false
    this.claimOrSignal ="reclamation"
    this.userIndex=index
    this.sendMessage("societe : "+this.users[index].nom)

    console.log(this.messages)
    this.hideTextZone=false
    //window.scrollTo(0,document.scrollingElement.scrollHeight);
    window.scrollTo(0,document.querySelector(".scrollingContainer").scrollHeight);
    
  }

  Contenu_de_reclamation(textMessage:string){
    this.sendMessage("Contenu : "+textMessage)
    this.claimOrSignal = ""

    this.hideTextZone=true

    this.reclamationService.sendClaim(this.users[this.userIndex].id,textMessage).subscribe(res=>{
      console.log(res)
    })

    var container = document.getElementById("msgContainer");           
    container.scrollTop = container.scrollHeight;  
  }

  Continuer(){
    this.sendMessage("je veux Continuer : ")
  }
  Quitter(){
    this.sendMessage("Merci, je veux Quitter")
  }

  /**************************************************************************** */

  /******************************signalisation*********************************** */

  ChoisirPersonne(){
    this.showsignalModal = true
    
  }
  Confirmer(){
    this.sendMessage("Confirmer signalisation ")
    this.reclamationService.sendSignal(this.users[this.userIndex].id,this.confirmationMessage ).subscribe(res=>{
      console.log(res)
    })
  }

  choix_de_condidat(index:any){
    this.showsignalModal=false
    this.claimOrSignal = "signalisation"
    this.userIndex=index
    this.sendMessage("Candidat : "+this.users[index].nom)

   // console.log(this.messages)
    this.hideTextZone=false
    //window.scrollTo(0,document.scrollingElement.scrollHeight);
   // window.scrollTo(0,document.querySelector(".scrollingContainer").scrollHeight);
    
  }
  Raison_signalisation(textMessage:string){
    this.sendMessage("Raison de signalisation : "+textMessage)
    this.confirmationMessage =textMessage
    this.claimOrSignal = ""

    this.hideTextZone=true

    console.log(this.claimOrSignal)

 
  }
  
    /**************************************************************************** */

    /******************************payement*********************************** */

    closeShowBilling(){
      this.showBilling = false
    }

    payer(){
      this.payementService.payer().subscribe(res =>{
        console.log(res)
      })
    }

     /**************************************************************************** */
  

   scroll() {
    //window.scrollTo(0,document.scrollingElement.scrollHeight);
    var container = document.getElementById("msgContainer");           
    container.scrollTop = container.scrollHeight;  
    //window.scrollTo(0,document.querySelector(".scrollingContainer").scrollHeight);
  }

  
  
}
