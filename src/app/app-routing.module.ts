import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StatistiquesComponent } from './statistiques/statistiques.component';
import { HomeComponent } from './home/home.component';
import { Error404Component } from './error404/error404.component';
import { SingInComponent } from './sing-in/sing-in.component';


const routes: Routes = [
  {path:'statistique', component : StatistiquesComponent},
  {path:'', component : HomeComponent},
  {path:'home', component : HomeComponent},
  {path:'signin', component : SingInComponent},
  {path:'error', component : Error404Component},
  {path:'**', redirectTo : 'error',pathMatch : 'full'}
];




@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
