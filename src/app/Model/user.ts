export class User {

    id: number;
    prenom: string;
    nom: string;
    adresse: string;
    mail: string;
    password: string;
    isActiv: boolean;
}
